package pe.edu.sise.android.cinema;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CinemaSiseApplication {
 
	public static void main(String[] args) {
		SpringApplication.run(CinemaSiseApplication.class, args);
	}
}
