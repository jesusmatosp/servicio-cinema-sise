package pe.edu.sise.android.cinema.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.sise.android.cinema.domain.Movie;
import pe.edu.sise.android.cinema.dto.MovieDTO;
import pe.edu.sise.android.cinema.repository.MovieRepository;

@Service
public class MovieServiceImpl implements MovieService {

	@Autowired
	public MovieRepository movieRepository;
	
	@Override
	public List<MovieDTO> getAllMovie() throws Exception {
		// TODO Auto-generated method stub
		List<Movie> lista= movieRepository.findAll();
		List<MovieDTO> listaDTO = new ArrayList<>();
		for(Movie movie:lista){
			MovieDTO movieDTO = new MovieDTO();
			BeanUtils.copyProperties(movieDTO, movie);
			listaDTO.add(movieDTO);
		}
		return listaDTO;
	}

	@Override
	public MovieDTO getMovieById(Long id) throws Exception {
		// TODO Auto-generated method stub
		Movie movie = movieRepository.findOne(id);
		MovieDTO movieDTO = new MovieDTO();
		BeanUtils.copyProperties( movieDTO , movie );
		return movieDTO;
	}

	@Override
	public MovieDTO saveMovie(MovieDTO movieDTO) throws Exception {
		// TODO Auto-generated method stub
		Movie movie = new Movie();
		BeanUtils.copyProperties(movie, movieDTO);
		movieRepository.save(movie);
		movieDTO.setId(movie.getId());
		return movieDTO;
	}

}
