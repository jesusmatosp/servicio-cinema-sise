package pe.edu.sise.android.cinema.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("response")
public class ResponseDTO {

	private Integer code;
	private String message;
	private List<MovieDTO> movie;
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<MovieDTO> getMovie() {
		return movie;
	}
	public void setMovie(List<MovieDTO> movie) {
		this.movie = movie;
	}

}
