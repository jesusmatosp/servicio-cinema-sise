package pe.edu.sise.android.cinema.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.edu.sise.android.cinema.dto.MovieDTO;
import pe.edu.sise.android.cinema.dto.ResponseDTO;
import pe.edu.sise.android.cinema.dto.ResponseMovieDTO;
import pe.edu.sise.android.cinema.service.MovieService;

@RestController
@RequestMapping("/movies")
public class MovieRestController {

	@Autowired 
	private MovieService movieService;
	
	@GetMapping("/list")
	public ResponseDTO getListMovie(){
		ResponseDTO response = new ResponseDTO();
		try {
			List<MovieDTO> movies = movieService.getAllMovie();
			response.setCode(HttpStatus.OK.value());
			response.setMessage(HttpStatus.OK.name());
			response.setMovie(movies);
		} catch (Exception e) {
			response.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.setMessage(e.getLocalizedMessage());
			e.getMessage();
		}
		return response;
	}
	
	@PostMapping("/get")
	public ResponseMovieDTO getMovie(@RequestBody MovieDTO movieDTO){
		ResponseMovieDTO responseMovieDTO = new ResponseMovieDTO();
		try {
			MovieDTO movie = movieService.getMovieById(movieDTO.getId());	
			responseMovieDTO.setCode(HttpStatus.OK.value());
			responseMovieDTO.setMessage(HttpStatus.OK.name());
			responseMovieDTO.setMovie(movie);
		} catch (Exception e) {
			responseMovieDTO.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			responseMovieDTO.setMessage(e.getLocalizedMessage());
			e.getMessage();
		}
		return responseMovieDTO;
	}
	
}
