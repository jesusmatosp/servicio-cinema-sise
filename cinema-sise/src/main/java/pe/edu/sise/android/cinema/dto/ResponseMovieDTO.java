package pe.edu.sise.android.cinema.dto;

import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("responseMovie")
public class ResponseMovieDTO {

	private Integer code;
	private String message;
	private MovieDTO movie;
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public MovieDTO getMovie() {
		return movie;
	}
	public void setMovie(MovieDTO movie) {
		this.movie = movie;
	}
}
