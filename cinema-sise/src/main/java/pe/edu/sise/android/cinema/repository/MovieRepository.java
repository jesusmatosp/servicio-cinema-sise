package pe.edu.sise.android.cinema.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pe.edu.sise.android.cinema.domain.Movie;

public interface MovieRepository extends JpaRepository<Movie, Long> {

}
