package pe.edu.sise.android.cinema.service;

import java.util.List;

import pe.edu.sise.android.cinema.dto.MovieDTO;

public interface MovieService {

	public List<MovieDTO> getAllMovie() throws Exception;
	public MovieDTO getMovieById(Long id) throws Exception;
	public MovieDTO saveMovie(MovieDTO movieDTO) throws Exception;
	
}
